// [SECTION] OBJECTS
/*
    - An object is a data type that is used to represent real world objects.
    - Information stored in objects are represented in a "key:value" pair
    - A 'key' is also mostly reffered to as a "property of an object"
    - Different data types may be stored in an object's property creating complex data structures.
    - Syntax: 
        let objectName = {
            keyA: valueA,
            keyB: valueB
        }
*/

let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: 1999
};

console.log('Result from creating objects');
console.log(cellphone);
console.log(typeof cellphone);




// The 'this' keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters.

function Laptop(name, manufactureDate, price) {
	this.name = name;
	this.manufactureDate = manufactureDate;
	this.price = price;

	// return {name: name, manufactureDate: manufactureDate, price: price}
}

// the 'new' operator creates an instance of an object.

let laptop = new Laptop('Lenovo', 2008, 20000);

console.log('Result from creating objects using object constructors:');
console.log(laptop);

let myLaptop = new Laptop('MacBook Air', 2020, 30000);

console.log('Result from creating objects using object constructors:');
console.log(myLaptop);

// [SECTION] Accessing Object Properties

// Using the dot notation
console.log('Result from dot notation: ' + myLaptop.name);

// Using the square bracket notation
console.log('Result from square bracket notation: ' + myLaptop['name'])

// Accessing array objects
/*
	- Accessing object properties using the bracket notation and indexes can cause confusion.
	- By using the dot notation, this easily helps us differentiate accessing elements from arrays and properties from objects.
*/

// let laptop = new Laptop('Lenovo', 2008, 20000);
// let myLaptop = new Laptop('MacBook Air', 2020, 30000);

let array = [laptop, myLaptop]

console.log(array[0]['price']);
console.log(array[1].name);

let sample = ['Hello', {firstName: 'John', lastName: 'Doe'}, 78];

console.log(sample[1]);
console.log(sample[1].firstName);
console.log(sample[2]);

// [Section] Initializing/Adding/Deleting/Reassigning Object Properties
/*
    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
    - This is useful for times when an object's properties are undetermined at the time of creating them
*/

let car = {};

// Initializing/adding object properties using dot notation
car.name = 'Honda Civic';

console.log('Result from adding properties using dot notation:');
console.log(car);

// Initializing/adding object properties using bracket notation
/*
    - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
- This also makes names of object properties to not follow commonly used naming conventions for them
*/

// Adding object properties

// car.manufacture_date = 2019;
// console.log(car);

car['manufacture date'] = 2019;

console.log('Result from adding properties using bracket notation:')
console.log(car);

// Deleting object properties
delete car['manufacture date'];

console.log('Result deleting properties:');
console.log(car);

// Reassigning object properties
car.name = 'Dodge Charger R/T';

console.log('Result from reassigning properties:')
console.log(car);


// [Section] Object Methods
/*
    - A method is a function which is a property of an object
    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
    - Methods are useful for creating object specific functions which are used to perform tasks on them
    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
*/

// Note: If you access an object method without (), it will return the function definition:

// function() { return this.firstName + " " + this.lastName; }

let person = {
    name: 'John',
    talk: function (){
        console.log('Hello my name is ' + this.name);
    }
}

console.log(person);
console.log('Result from object methods:');
person.talk();

// Adding methods to objects
person.walk = function() { 
    console.log(this.name + ' walked 25 steps forward.');
};

console.log(person);
person.walk();

// // Methods are useful for creating reusable functions that perform tasks related to objects
let friend = {
    firstName: 'Joe',
    lastName: 'Smith',
    address: {
        city: 'Austin',
        country: 'Texas'
    },
    emails: ['joe@mail.com', 'joesmith@email.xyz'],
    introduce: function() {
        console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
    }
};

friend.introduce();

console.log(friend.address)
console.log(friend.address.city)
console.log(friend.emails[1])
console.log(friend.emails)


// [SECTION] Real World Application Objects

/*
	- Scenario
	1. We would like to create a game that would have pokemon interact with each other.
	2. Every pokemon would have the same set of stats, properties and function.
*/

  // Creating an object constructor instead will help with this process

	

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        };
    this.faint = function(){
        console.log(this.name + 'fainted.');
    }

}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 3);
let rattata = new Pokemon('Rattata', 8);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(rattata);